import { NgModule } from '@angular/core';

import { DatasourceService } from '../services/datasource.service';
import { ProductService } from '../services/product.service';

@NgModule({
  providers: [DatasourceService, ProductService]
})
export class ModelModule {}
