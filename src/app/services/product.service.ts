import { Injectable } from '@angular/core';
import { Product } from '../model/product.model';
import { DatasourceService } from './datasource.service';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  private products: Product[] = [];
  private categories: string[] = [];

  constructor(private dataSource: DatasourceService) {
    dataSource.getAllProducts().subscribe(data => {
      this.products = data;
      this.categories = data
        .map(el => el.category)
        .filter((category, idx, array) => array.indexOf(category) === idx)
        .sort();
    });
  }

  getProducts(category: string = null): Product[] {
    return this.products.filter(
      p => category === null || category === p.category
    );
  }

  getProduct(id: number): Product {
    return this.products.find(el => el.id === id);
  }

  getCategories(): string[] {
    return this.categories;
  }
}
